//moveable box

int y2 = 250;
int y1 = 250;

int x=100;
int y=150;
int dx=5;
int dy=5;

int player1 = 0;
int player2 = 0;

int batWidth = 60;
int ballSize = 20;
int batSpeed = 25;
int dificulty = 10;

void setup()
{
 size(1000, 500);
 frameRate(dificulty*6);
}
void draw()
{
 background(0);
 fill(0,255,0);
 rect(40,y1,20,batWidth);
 rect(width - 70,y2,20,batWidth);
 
 ellipse( x, y, ballSize, ballSize);
 
 text(player1, width/2,10);
 
  y2 = y - batWidth/2;
  x=x+dx;
  y=y+dy;
  
  if ( x < ballSize/2) {
  dx = dx*-1;
  x = width/2;
  player1 = player1+1;
  }
  
  if ( x > width - ballSize/2) {
  dx = dx*-1;
  x = width/2;
  }
  
  if ( y < ballSize/2) {
  dy = dy*-1;
  }
  
  if ( y > height - ballSize/2) {
  dy = dy*-1;
  }
  
  
  if (( (x < 70 && x > 40) && y > y1 && y < (y1+batWidth))) {
    dx = dx*-1;
  }
  
  if ( (x > width - 70 && x < width - 40) && y > y2 && y < (y2+batWidth)) {
    dx = dx*-1;
  }
  
}

void keyPressed()
{
  if(key == CODED)
  {
    if(keyCode == UP && y1 > 0 )
    {
      y1 = y1 - batSpeed;
    }
    if(keyCode == DOWN && y1 < height - batWidth)
    {
      y1 = y1 + batSpeed;
    }
  }
  if(key == 'w' && y2> 0 )
    {
      y2 = y2 - batSpeed;
    }
  if(key == 's' && y2 < height - batWidth)
    {
      y2 = y2 + batSpeed;
    }
}
      